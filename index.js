// fetch request to retrieve all items
fetch("https://jsonplaceholder.typicode.com/todos")
  .then((res) => res.json())
  .then((json) => console.log(json));

// title array using map method
fetch("https://jsonplaceholder.typicode.com/todos")
  .then((res) => res.json())
  .then((json) =>
    json.map((getTitle) => {
      return getTitle.title;
    })
  )
  .then((data) => console.log(data));

// fetch request to retrieve single item
fetch("https://jsonplaceholder.typicode.com/todos/3")
  .then((res) => res.json())
  .then((json) => console.log(json));

// print a message in the console that provides the title and status of the item
fetch("https://jsonplaceholder.typicode.com/todos/3")
  .then((res) => res.json())
  .then((json) =>
    console.log(`title: ${json.title} isCompleted: ${json.completed}`)
  );

// fetch request using POST to create an item
fetch("https://jsonplaceholder.typicode.com/todos", {
  method: "POST",
  headers: { "Content-Type": "application/json" },
  body: JSON.stringify({
    userId: 11,
    id: 201,
    title: "created item",
    completed: true,
  }),
})
  .then((res) => res.json())
  .then((json) => console.log(json));

// fetch request using PUT to update an item
fetch("https://jsonplaceholder.typicode.com/todos/7", {
  method: "PUT",
  headers: { "Content-Type": "application/json" },
  body: JSON.stringify({
    userId: 1,
    id: 5,
    title: "updated item",
    completed: false,
  }),
})
  .then((res) => res.json())
  .then((json) => console.log(json));

// update an item by changing the data structure
fetch("https://jsonplaceholder.typicode.com/todos/6", {
  method: "PUT",
  headers: { "Content-Type": "application/json" },
  body: JSON.stringify({
    userId: 12,
    title: "updated item 2",
    description: "coding",
    isCompleted: true,
    dateCompleted: "10/25/2021",
  }),
})
  .then((res) => res.json())
  .then((json) => console.log(json));

// fetch request using PATCH method to update an item
fetch("https://jsonplaceholder.typicode.com/todos/7", {
  method: "PATCH",
  headers: { "Content-Type": "application/json" },
  body: JSON.stringify({
    userId: 6,
    id: 7,
    title: "updated item using patch",
  }),
})
  .then((res) => res.json())
  .then((json) => console.log(json));

// update by changing the status to complete and add a date when the status was changed
fetch("https://jsonplaceholder.typicode.com/todos/9", {
  method: "PATCH",
  headers: { "Content-Type": "application/json" },
  body: JSON.stringify({
    completed: true,
    dateCompleted: "10/24/2021",
  }),
})
  .then((res) => res.json())
  .then((json) => console.log(json));
